﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Models;

namespace Web.Api.Tests.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private IEnumerable<Customer> Customers;

        private readonly FakeDataLoader Data;
        public CustomerController()
        {
            Data = new FakeDataLoader();
        }


        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int count)
        {
            Customers = await Data.TryGet<Customer>(count);
            if(Customers==null)
                return BadRequest();
            return Ok(Customers);
        }
    }
}