﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Models;

namespace Web.Api.Tests
{
    /// <summary>
    /// Fake generate a load of Customers.
    /// </summary>
    public class FakeDataLoader
    {
        public List<Customer> Customers
        {
            get; private set;
        } = new List<Customer>();

        public FakeDataLoader() { }


        public async Task<IEnumerable<Customer>> TryGet<T>(int count)
        {
            await GenerateCustomers(count);
            return Customers.ToList();
        }

        private async Task<bool> GenerateCustomers(int count)
        {
            bool result = false;
            string[] genders = { "Male", "Female" };

            await Task.Factory.StartNew(() =>
            {
                var rand = new Random();
                for (int i = 0; i < count; i++)
                {
                    int index = rand.Next(genders.Length);
                    Customers.Add(new Customer(genders[index], i));
                }
            }).ContinueWith(f => {
                result = f.IsCompleted;
            });

            return result;
        }
    }
}
