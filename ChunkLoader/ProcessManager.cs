﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ChunkLoader
{
    public class ProcessManager
    {
        private static ProcessManager Instance;

        private ProcessManager() { }

        public static ProcessManager CreateInstance()
        {
            if (Instance == null)
                Instance = new ProcessManager();
            return Instance;
        }

        public HttpClient HttpClient
        {
            get { 
                var client = new HttpClient();
                var header = new MediaTypeWithQualityHeaderValue("application/json");
                //Add security, Cors, etc..
                client.DefaultRequestHeaders.Accept.Add(header);
                return client;
            }
        }
    }
}
