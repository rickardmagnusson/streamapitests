﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Models;

namespace ChunkLoader
{
    class Program
    {
        private readonly static string url = "https://localhost:44343/customer?count=100";

        static void Main(string[] args)
        {
            var task = GetCustomersAsync<Customer>()
                .GetAwaiter()
                    .GetResult();

            Task.Run(async () => await ProcessAsync(task));

            Console.ReadLine();
        }


        public static async Task<IEnumerable<T>> GetCustomersAsync<T>()
        {
            return await Task.Run(() =>
                ProcessManager.CreateInstance()
                    .HttpClient
                        .GetValues<T>(url));
        }


        public static async Task ProcessAsync<T>(IEnumerable<T> left) 
        {
            await Task.Yield();

            left.ToList().ChunkBy(10)
                .ForEach(async f =>{
                    await PrintAsync(f); 
                });
        }


        public static async Task PrintAsync<T>(List<T> list)
        {
            await Task.Yield();

            Console.WriteLine("Start process batch...");

            list.ForEach(c => {
                var client = c as Customer;
                Console.WriteLine($"Inserted: ---> {client.Number}: {client.FirstName} {client.LastName}");
            });
            
            Console.WriteLine("End process batch.");
            Console.WriteLine("");

            
        }
    }
}
